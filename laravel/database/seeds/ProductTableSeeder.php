<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new \App\Product([
            'imagePath' => 'img/meilong3x3.jpg',
            'title' => 'Cubo MeiLong 3x3 M',
            'description' => 'El MeiLong es la version magnética del MeiLong base. 
                              Este cubo es bastante popular por su bajo precio y su alto rendimiento.',
            'price' => 7,
            'stock' => 45,
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'img/MoyuAosuGTSv2M.jpg',
            'title' => 'Cubo MoYu AoSu GTS V2 M',
            'description' => 'El MoYu AoSu GTS V2 M viene con las últimas mejores en speedcubing, una alta calidad con un mejor precio.
                              Usado por los mejores en el speedcubing como Xuming Wang en 23.37 seg.',
            'price' => 43,
            'stock' => 10,
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'img/cubo_para_ciegos.jpg',
            'title' => 'Cubo de rubik táctil ',
            'description' => 'Cubo de rubik pensado para personas ciegas o con baja visión, adaptado con relieve en cada cara.',
            'price' => 30,
            'stock' => 0,
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'img/puzzle.png',
            'title' => 'Puzzle Britto 5000 piezas',
            'description' => 'Puzzle de la marca Britto con las mejores piezas.',
            'price' => 40,
            'stock' => 3,
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'img/puzzle500.jpg',
            'title' => 'Puzzle 500 Educa',
            'description' => 'Puzzle de 500 piezas de la marca mas conocida de puzzles con una calidad superior a la competencia.',
            'price' => 10,
            'stock' => 35,
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'img/shengshou7x7.jpg',
            'title' => 'Cubo ShengShou 7x7',
            'description' => 'El shengshou 7x7 ha sido uno de los mejores cubos de Rubik 7x7, 
                                tiene un giro suave y cómodo además tiene un buen corte de esquinas.',
            'price' => 27,
            'stock' => 45,
        ]);
        $product->save();
        
        $product = new \App\Product([
            'imagePath' => 'img/hanayama-cast-rotor.jpg',
            'title' => 'Hanayama Cast Rotor',
            'description' => 'Las piezas, que tienen la misma forma y se entrelazan una en otra, presenta un elegante diseño. 
                                Su particular estructura y las numerosas soluciones posibles hacen que sea un puzzle difícil.',
            'price' => 13,
            'stock' => 10,
        ]);
        $product->save();
       
        $product = new \App\Product([
            'imagePath' => 'img/puzzle6000.jpg',
            'title' => 'Puzzle 6000 piezas Educa',
            'description' => 'Puzzle de 6000 piezas que representa a muchos animales salvajes saliendo de cuadros y colándose en una habitación.',
            'price' => 50,
            'stock' => 0,
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'img/puzzlePokemon1000.jpg',
            'title' => 'Puzzle 1000 piezas Pokemon, Ravensburger',
            'description' => 'Puzzle de 1000 piezas de todos los pokemons de primera generación.',
            'price' => 15,
            'stock' => 16,
        ]);
        $product->save();
        
        $product = new \App\Product([
            'imagePath' => 'img/gans356airM.jpg',
            'title' => 'Cubo Gan 356 Air M',
            'description' => 'El GAN 356 Air M es uno de los mejores Cubos de Rubik 3x3 del mercado, lleva un sistema de posicionamiento 
                            magnético y cuenta con ranuras especiales en el interior de sus piezas para sujetar los imanes.',
            'price' => 40,
            'stock' => 25,
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'img/4x4.jpg',
            'title' => 'Cubo ShengShou 4x4',
            'description' => 'El ShengShou se corona como uno de los mejores 4x4 del mercado con el precio mas bajo.',
            'price' => 10,
            'stock' => 85,
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'img/hanayamaCastTwist.jpg',
            'title' => 'Hanayama Cast Twist',
            'description' => 'Los surcos del Hanayama Cast Twist te permiten desplazarte por los resaltes de este rompecabezas para intentar
                                completar este complicado puzzle.',
            'price' => 13,
            'stock' => 5,
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'img/puzzle3000.jpg',
            'title' => 'Puzzle 3000 piezas Educa',
            'description' => 'Puzzle de 3000 piezas de la marca Educa con una imagen de la Torre Eiffel.',
            'price' => 35,
            'stock' => 14,
        ]);
        $product->save();

       

        $product = new \App\Product([
            'imagePath' => 'img/puzzle9000.jpg',
            'title' => 'Puzzle 9000 piezas Gru, Ravensburger',
            'description' => 'Puzzle de 9000 piezas de la marca Ravensburger, con imagenes de la película: Gru mi villano favorito.',
            'price' => 98,
            'stock' => 0,
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'img/moyu-weilong-gts-v2-3x3-magnetico.jpg',
            'title' => 'Cubo MoYu WeiLong GTS V2 M',
            'description' => 'El actual récord del mundo de single, con un tiempo de 3.47 seg, esta realizado con este mismo cubo.',
            'price' => 23,
            'stock' => 50,
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'img/shengShou13x13.jpg',
            'title' => 'Cubo ShengShou 13x13',
            'description' => 'El ShengShou 13x13 es un cubo que ha pesar de ser muy grande su peso no es excesivo, pesa alrededor de 1Kg 
                                lo que lo hace bastante manejable.',
            'price' => 230,
            'stock' => 1,
        ]);
        $product->save();

        $product = new \App\Product([
            'imagePath' => 'img/puzzleRavensburger9000.jpg',
            'title' => 'Puzzle 9000 piezas, Ravensburger',
            'description' => 'Puzzle de 9000 piezas de la marca Ravensburger con una fotografia de una batalla de barcos.',
            'price' => 80,
            'stock' => 3,
        ]);
        $product->save();


        $product = new \App\Product([
            'imagePath' => 'img/rompecabezas.png',
            'title' => 'Hanayama Cast Duet',
            'description' => 'El rompecabezas Hanayama Cast Duet puede parecer a primera vista que un anillo ha sido colgado de una cuadrícula, 
                                pero en realidad hay dos anillos qe están unidos por imanes.',
            'price' => 13,
            'stock' => 5,
        ]);
        $product->save();

    }
}
