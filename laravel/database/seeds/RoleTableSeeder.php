<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new \App\Role([
            'id' => '1',
            'nombre_rol' => 'administrador'
        ]);
        $role->save();

        $role = new \App\Role([
            'id' => '2',
            'nombre_rol' => 'usuario'
        ]);
        $role->save();
    }
}
