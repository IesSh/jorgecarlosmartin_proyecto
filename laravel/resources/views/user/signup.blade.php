@extends('layouts.master')

@section('title')
Registrar
@endsection

@section('styles')
<style>
.row_signin{
  width: 100%;
  background: white;
  border-radius: 30px;
  box-shadow: 12px 12px 22px grey;
}

.img_sign{
  margin-left: -4%;
  border-top-left-radius: 30px;
  border-bottom-left-radius: 30px;
}

.boton_sign{
  border:none;
  outline: none;
  height: 50px;
  width: 100%;
  background-color: black;
  color:white;
  border-radius: 4px;
  font-weight: bold;
}

.boton_sign:hover{
  background: white;
  border: 1px solid;
  color:black;
}

.logoimagen{
  margin-left: -3%;
}

.alertaLogin{
  margin: auto;
  width: 45%;
  text-align: center;
}

/* Para cambiar el color de los placeholders del formulario */
#name::placeholder,
#email::placeholder,
#password::placeholder,
#address::placeholder,
#city::placeholder,
#zip::placeholder
  {
  color:#989898;
}

#name,
#email,
#password,
#address,
#city,
#zip
  {
  color:#101010;
}
/* FIN placeholders */


</style> 
@endsection
@section('content')

<!-- Errores y mensajes -->
@error('name')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      El nombre no es válido
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('email')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
        El email ya está registrado
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('password')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      La contraseña no es válida
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('address')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      La dirección no es válida
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('city')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      La ciudad no es válida
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror

@error('zip')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      El código postal no es válido
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror
<!-- FIN Errores y mensajes -->

<!-- Section formulario crear cuenta -->
<section class="Form mb-5 mt-5">
  <div class="container">
    <div class="row row_signin no-gutter">
      <div class="col-lg-5">
          <img src="{{ URL::to('img/erno.jpg') }}" alt="imagen" class=" img_sign img-fluid">
      </div>
      <div class="col-lg-7 px-5 pt-5">
        <h1 class="font-weight-bold py-3"><img src="{{ URL::to('img/logo_v1.png') }}" class="logoimagen" style="max-height: 53px" alt="Logo"></h1>
        <h3>Crea tu cuenta</h3>
        <form action="{{ route('user.signup') }}"  class="needs-validation" method="post">
          <div class="form-row">
            <div class="col-lg-6 mb-4">
            <label for="inputname">Nombre </label>
              <input type="name" placeholder="Nombre" id="name"  name="name" class="form-control x p-4 " value="{{ old('name') }}">
            </div>

            <div class="col-lg-6 mb-4">
            <label for="inputEmail">Email</label>
              <input type="email" placeholder="email@email.com" id="email" name="email" class="form-control x p-4" value="{{ old('email') }}">
            </div>
          </div>
          <div class="form-row">
            <div class="col-lg-6 mb-4">
            <label for="inputPassword4">Contraseña</label>
              <input type="password" placeholder="Contraseña" id="password" name="password" class="form-control x p-4">
            </div>
            <div class="col-lg-6 mb-4">
            <label for="inputAddress">Dirección</label>
              <input type="text" placeholder="Calle Ejemplo Nº Piso " id="address" name="address" class="form-control x p-4" value="{{ old('address') }}">
            </div>
          </div>
          <div class="form-row">
            <div class="col-lg-6 mb-4">
            <label for="inputCity">Ciudad</label>
              <input type="text" placeholder="Cuidad" id="city" name="city" class="form-control x p-4" value="{{ old('city') }}">
            </div>
            <div class="col-lg-6 mb-4">
            <label for="inputCity">Código Postal</label>
              <input type="text" placeholder="Código Postal" id="zip" name="zip" class="form-control x p-4" value="{{ old('zip') }}">
            </div>
          </div>
          <div class="form-row">
            <div class="col-lg-12">
              <button type="submit" class="btn1 boton_sign mt-3 mb-4">Registrarse</button>
            </div>
          </div>
          <p>¿Ya tienes cuenta? - <a href="{{ route('user.signin') }}">Inicia sesión</a></p>
          {{ csrf_field() }}
        </form>
      </div>
    </div>
  </div>
</section>
<!-- FIN Section formulario crear cuenta -->
@endsection