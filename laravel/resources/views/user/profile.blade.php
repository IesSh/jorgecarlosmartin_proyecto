@extends('layouts.master')

@section('title')
Perfil
@endsection

@section('styles')

<style>
.row_signin{
  width: 100%;
  background: white;
  box-shadow: 12px 12px 22px grey;
}
.boton_sign{
  border:none;
  outline: none;
  height: 50px;
  width: 100%;
  background-color: black;
  color:white;
  border-radius: 4px;
  font-weight: bold;
}

.boton_sign:hover{
  background: white;
  border: 1px solid;
  color:black;
}

.nombreinput{
  margin-top: 15%;
}

</style>
@endsection

@section('content')

<!-- BreadCrumps -->
<nav aria-label="breadcrumb">
  <ol class="breadcrumb mt-2">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Perfil de {{ Auth::user()->name }}</li>
  </ol>
</nav>
<!-- FIN BreadCrumps -->

<!-- SECTION DEL PERFIL -->
<section class="Form mb-5 mt-5">
  <div class="container">
    <div class="row row_signin no-gutter">
      <div class="col-lg-12 px-5 pt-5">
        <h1 class="font-weight-bold py-3"><img src="{{ URL::to('img/logo_v1.png') }}" class="logoimagen" style="max-height: 53px" alt="Logo"></h1>
        <h2>Perfil</h2>
        <form action="{{ route('user.signup') }}"  class="needs-validation" method="post">
          <div class="form-row">
            <div class="col-lg-6 mb-4">
            <h4>Nombre</h4>
              <div>{{ Auth::user()->name }}</div>
            </div>
            <div class="col-lg-6 mb-4">
            <h4>Email</h4>
              <div>{{ Auth::user()->email }}</div>
            </div>
          </div>
          <div class="form-row">
            <div class="col-lg-12 mb-4">
            <h4>Dirección</h4>
            <div>{{ Auth::user()->address }}</div>
            </div>
          </div>
          <div class="form-row">
            <div class="col-lg-6 mb-4">
            <h4>Ciudad</h4>
            <div>{{ Auth::user()->city }}</div>
            </div>
            <div class="col-lg-6 mb-4">
            <h4>Código Postal</h4>
            <div>{{ Auth::user()->zip }}</div>
            </div>
          </div>
          {{ csrf_field() }}
        </form>
        <a href="{{ route('user.edit', Auth::user()->id) }}"><button type="submit" class="btn1 boton_sign mt-3 mb-4">Editar Perfil</button></a>
      </div>
    </div>
  </div>
</section>
<!-- FIN SECTION DEL PERFIL -->
@endsection