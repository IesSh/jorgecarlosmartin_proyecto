<footer class="footer mt-auto py-3">
  <div class="container">
  <div class="row">
          <div class="col-12 col-md">
          <img src="{{ URL::to('img/logo_v1.png') }}" style="max-height: 50px" alt="Logo">
            <div>Tu tienda de Cubos de Rubik, Puzzles y todo lo que puedas imaginar.</div>
          </div>
          <div class="col-6 col-md">
            <h5><i class="fas fa-info"></i> Informacion sobre la tienda </h5>
            <ul class="list-unstyled text-small">
              <li>Witchercubes.com, Calle falsa N123, 42040, (Zaragoza), España</li>
              <li>Llamanos: <a class="footerNumEmail" href="{{ route('loading.index') }}">694 584 123</a></li>
              <li>Email: <a class="footerNumEmail" href="{{ route('loading.index') }}">info@witchercubes.com</a> </li>
              <li></li>
              <li></li>
              <li></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Quienes Somos <i class="fas fa-users"></i></h5>
            <ul class="list-unstyled text-small">
              <li><a class="" href="{{ route('loading.index') }}">Aviso Legal</a></li>
              <li><a class="" href="{{ route('loading.index') }}">Condiciones legales</a></li>
              <li><a class="" href="{{ route('loading.index') }}">Política de privacidad</a></li>
              <li><a class="" href="{{ route('loading.index') }}">Política de devoluciones</a></li>
              <li><a class="" href="{{ route('loading.index') }}">Blog</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Redes Sociales <i class="fas fa-hashtag"></i></h5>
            <ul class="list-unstyled text-small">
              <li><a class="" href="https://www.instagram.com">Instagram <i class="fab fa-instagram"></i></a></li>
              <li><a class="" href="https://www.facebook.com">Facebook <i class="fab fa-facebook-f"></i></a></li>
              <li><a class="" href="https://www.twitter.com">Twitter <i class="fab fa-twitter"></i></a></li>
              <li><a class="" href="https://www.youtube.com">Youtube <i class="fab fa-youtube"></i></a></li>
            </ul>
          </div>
        </div>
  </div>
</footer>