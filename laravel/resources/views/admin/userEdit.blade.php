@extends('admin.master')

@section('title')
Admin Witcher's
@endsection

@section('styles')
@endsection

@section('content')
<h2>Editar {{ $user->name }}</h2>

<!-- Errores y mensajes -->
@if (session('mensaje'))
<div class="alert alert-success mt-3">
    {{ session('mensaje') }}
</div>
@endif

@error('name')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      El nombre no es válido
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror
@error('email')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
        El email ya esta registrado
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror
@error('address')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      La dirección no es válida
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror
@error('city')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      La ciudad no es válida
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror
@error('zip')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      El codigo postal no es válido
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror
@error('role_id')
  <div class="alertaLogin alert alert-danger alert-dismissible fade show mt-2" role="alert">
      El role_id no es válido
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@enderror
<!-- FIN Errores y mensajes -->

<!-- Formulario editar usuario -->
<form action="{{ route('admin.userUpdate', $user->id) }}" method="POST">
    @method('PUT')
    @csrf

    <div class="form-row">
      <div class="col-lg-6 mb-4">
        <label for="inputname">Nombre </label>
        <input type="name"  id="name"  name="name" class="form-control x p-4 " value="{{ $user->name }}">
      </div>
      <div class="col-lg-6 mb-4">
        <label for="inputEmail">Email</label>
        <input type="email"  id="email" name="email" class="form-control x p-4" value="{{ $user->email }}">
      </div>
    </div>

    <div class="form-row">
      <div class="col-lg-6 mb-4">
        <label for="inputAddress">Dirección</label>
        <input type="text" id="address" name="address" class="form-control x p-4" value="{{ $user->address }}">
      </div>
      <div class="col-lg-6 mb-4">
        <label for="inputCity">Ciudad</label>
        <input type="text" id="city" name="city" class="form-control x p-4" value="{{ $user->city }}">
      </div>
    </div>

    <div class="form-row">
      <div class="col-lg-6 mb-4">
        <label for="inputZip">Código Postal</label>
        <input type="text" id="zip" name="zip" class="form-control x p-4" value="{{ $user->zip }}">
      </div>
      <div class="col-lg-6 mb-4">
        <label for="inputRole_id">Role_id</label>
        <input type="number" name="role_id" class="form-control" value="{{ $user->role_id }}" >
        <small>¡Importante!, El rol 1 es solo para administradores, el 2 para usuarios ( viene el 2 por defecto ).</small>
      </div>
    </div>      
  <button type="submit" class="btn btn-info">Editar</button>
</form>
<!-- FIN Formulario editar usuario -->
@endsection