@extends('admin.master')

@section('title')
Admin Witcher's
@endsection

@section('styles')
@endsection

@section('content')
<h2>Crear producto</h2>

<!-- Errores y mensajes -->
@if (session('mensaje'))

<div class="alert alert-success">
    {{ session('mensaje') }}
</div>

@endif
@error('title')
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        El titulo es obligatorio
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@enderror

@error('description')
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        La descripcion es obligatoria
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@enderror

@error('price')
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        El precio es obligatorio o numero no válido
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@enderror

@error('stock')
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        El stock es obligatorio o numero no válido
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@enderror
@error('file')
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        El file es obligatorio 
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@enderror
<!-- FIN Errores y mensajes -->

<!-- Formulario crear productos -->
<form action="{{ route('admin.create') }}" method="POST" enctype="multipart/form-data">
    @csrf
    {{ csrf_field() }}
  <div class="form-group">
    <label>Titulo</label>
    <input type="text" name="title" class="form-control" value="{{ old('title') }}" >
  </div>
  <div class="form-group">
    <label>Descripción</label>
    <input type="text" name="description" class="form-control" value="{{ old('description') }}">
  </div>
  <div class="form-group">
    <label>Precio</label>
    <input type="number" name="price" class="form-control" value="{{ old('price') }}">
  </div>
  <div class="form-group">
    <label>Stock</label>
    <input type="number" name="stock" class="form-control" value="{{ old('stock') }}">
  </div>
  <div class="form-group">
    <label>Imagen</label>
    <input accept="image/*" name="file" type="file" value="file">
  </div>
  <button type="submit" class="btn btn-primary">Crear</button>
</form>
<!-- FIN Formulario crear productos -->

@endsection