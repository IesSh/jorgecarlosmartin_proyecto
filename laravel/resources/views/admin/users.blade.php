@extends('admin.master')

@section('title')
Admin Witcher's
@endsection

@section('styles')
<style> 

.TablaAdmin{
  color:#d4af37;
}

</style> 
@endsection

@section('content')
<h1 class="admin m-3">ESTAS EN LA PAGINA DE ADMINISTRADOR, CUIDADO CON LO QUE TOCAS</h1>

<!-- Si no hay usuarios -->
@if($users->isEmpty())
  <h2>¡No hay usuarios!</h2>
@else

<!-- Si hay usuarios -->

<!-- Mensajes -->
@if (session('mensaje'))
<div class="alert alert-success mt-3">
    {{ session('mensaje') }}
</div>
@endif
<!-- FIN Mensajes -->

<!-- Tabla para mostrar usuarios -->
<table class="table">
    <thead class="thead-dark">
        <tr>
            <th>Nombre</th>
            <th>Email</th>
            <th>Role_id</th>
            <th>Dirección</th>
            <th>Ciudad</th>
            <th>Código Postal</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <!-- Si eres administrador, se cambia el color  -->
        @if($user->role_id == 1)
        <tr class="TablaAdmin">
            <td scope="row">{{ $user->name }}</td>
            <td>{{ $user->email }} </td>
            <td>{{ $user->role_id }}</td>
            <td>{{ $user->address }}</td>
            <td>{{ $user->city }}</td>
            <td>{{ $user->zip }} 
            <form action="{{ route('admin.userDelete', $user) }}" method="POST" class="d-inline">
                  @method('DELETE')
                  @csrf
                    <button class="btn btn-danger float-right" type="submit">Eliminar</button>
                </form>
                <a href="{{ route('admin.userEdit', $user) }}" class="btn btn-info mx-2 float-right">Editar</a></td>
            @else
            <!-- Para los demas usuarios  -->
            <tr>
            <td scope="row">{{ $user->name }}</td>
            <td>{{ $user->email }} </td>
            <td>{{ $user->role_id }}</td>
            <td>{{ $user->address }}</td>
            <td>{{ $user->city }}</td>
            <td>{{ $user->zip }} 
            <form action="{{ route('admin.userDelete', $user) }}" method="POST" class="d-inline">
                  @method('DELETE')
                  @csrf
                    <button class="btn btn-danger float-right" type="submit">Eliminar</button>
                </form>
                <a href="{{ route('admin.userEdit', $user) }}" class="btn btn-info mx-2 float-right">Editar</a></td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
<!-- FIN Tabla para mostrar usuarios -->
@endif
@endsection
