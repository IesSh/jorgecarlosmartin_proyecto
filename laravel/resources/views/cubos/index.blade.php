@extends('layouts.master')

@section('title')
Witcher's
@endsection

@section('styles')

<style>
.titulo{
  font-weight: bold;
}

.subtitulo{
  color: #505050;
}

</style> 
@endsection

@section('content')

<!-- Buscador  -->
<nav class="navbar navbar-light float-right ">
  <form class="form-inline">
  <select name="tipo" class="form-control mr-lg-2">
    <option value="title">Nombre</option>
    <option value="description">Descripcion</option>
  </select>

    <input name="buscarpor" class="form-control mr-sm-2" type="search" placeholder="Buscar por nombre" aria-label="Search">
       <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Buscar</button>
       
  </form>
  <a href="/cubo"><button class="btn btn-outline-primary ml-1 my-2 my-sm-0" type="submit">Resetear Filtros</button></a>
</nav>

<!-- FIN Buscador  -->

<!-- BreadCrumps -->
<nav aria-label="breadcrumb">
  <ol class="breadcrumb mt-2">
    <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
    <li class="breadcrumb-item active" aria-current="page">Cubos</li>
  </ol>
</nav>
<!-- FIN BreadCrumps -->

<!-- Si no hay ningun producto se muestra el error -->
@if($products->isEmpty())

  <h2>¡No hay productos disponibles!</h2>

@else
<!-- Si hay productos -->

<h6 class="m-2">
    @if($buscarpor)
    <div class="alert alert-primary" role="alert">
        Los resultados de la busqueda '{{ $buscarpor }}' son:
    </div> 
    @endif
</h6>

<!-- Muestra de todos los productos disponibles -->
<div class="row col-lg-6 offset-lg-1">
<h3 class="titulo ">CUBOS DE RUBIK, PUZZLES Y MUCHO MAS...</h3>
<h6 class="subtitulo mb-4">
Tienda de venta online de cubos de rubik, puzzles y rompecabezas. Los mejores precios y el mejor servicio.
</h6>
</div>
@foreach($products->chunk(3) as $productChunk)
<div class="row row-cols-1 row-cols-md-3 row-cols-lg-4 offset-lg-1">
    @foreach($productChunk as $product)
    <div class="col mb-4 lg-3">
        <div class="card">
            <a href="{{ route('cubo.buy', $product) }}">
            <img src="{{ $product->imagePath }}"
                 class="card-img-top rounded my-3 mx-auto d-block"
                 alt="ImagenProducto"
                 onerror="this.src='img/errorImage.png'"></a>
            <div class="card-body">
                <a href="{{ route('cubo.buy', $product) }}"> <h5 class="card-title">{{ $product->title }}</h5></a>
                <p class="card-text ">{{ $product->description }}</p>
                <div class="price">{{ $product->price }}€</div>
                @if($product->stock != 0)
                <div class="stock">En stock</div>
                @else
                <div class="nostock">Sin stock</div>
                @endif
            </div>
        </div>
    </div>
    @endforeach
</div>
@endforeach
@endif
<!-- FIN Muestra de todos los productos disponibles -->
@endsection