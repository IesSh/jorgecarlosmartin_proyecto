@extends('layouts.master')

@section('title')
Witcher's
@endsection

@section('styles')
<style>
.estrella {
	color:orange;
}

.todoBuy {
	margin-top:2%;
}
</style> 
@endsection

@section('content')

<!-- BreadCrumps -->
<nav aria-label="breadcrumb">
  <ol class="breadcrumb mt-2">
    <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="/cubo">Cubos</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $products->title }}</li>
  </ol>
</nav>
<!-- FIN BreadCrumps -->

<!-- Mostrar el producto -->
<div class="content-wrapper todoBuy col-lg-12">	
	<div class="item-container ">	
		<div class="offset-lg-2">
		<img src="{{ URL::to($products->imagePath) }}" 
			 id="imagenbuy" 
			 class="imagenbuy offset-lg-3 mx-5 float-left rounded "
			 alt="ImagenProducto">
		</div>
		<div class="container col-md-7 col-lg-7 offset-lg-5">	
			<div>
				<h2><div class="product-title">{{ $products->title }}</div></h2>
				<div class="product-desc">{{ $products->description }}</div>
				<hr>
				<div class="product-price">{{ $products->price }} €</div>
				<!-- Para que las estrellas sean aleatorias -->
				@php  
				$numeroRandom = rand(1,5);
				@endphp
				@if($numeroRandom==1)
				<span class="fa fa-star estrella"></span>
				<span class="fa fa-star "></span>
				<span class="fa fa-star "></span>
				<span class="fa fa-star "></span>
				<span class="fa fa-star"></span>
				@endif
				@if($numeroRandom==2)
				<span class="fa fa-star estrella"></span>
				<span class="fa fa-star estrella"></span>
				<span class="fa fa-star "></span>
				<span class="fa fa-star "></span>
				<span class="fa fa-star"></span>
				@endif
				@if($numeroRandom==3)
				<span class="fa fa-star estrella"></span>
				<span class="fa fa-star estrella"></span>
				<span class="fa fa-star estrella"></span>
				<span class="fa fa-star "></span>
				<span class="fa fa-star"></span>
				@endif
				@if($numeroRandom==4)
				<span class="fa fa-star estrella"></span>
				<span class="fa fa-star estrella"></span>
				<span class="fa fa-star estrella"></span>
				<span class="fa fa-star estrella"></span>
				<span class="fa fa-star"></span>
				@endif
				@if($numeroRandom==5)
				<span class="fa fa-star estrella"></span>
				<span class="fa fa-star estrella"></span>
				<span class="fa fa-star estrella"></span>
				<span class="fa fa-star estrella"></span>
				<span class="fa fa-star estrella"></span>
				@endif
				<!-- Fin de las Estrellas aleatorias -->
				<!-- Para que no puedas comprar nada sin stock -->
				@if($products->stock != 0)
			<div class="stock">En stock</div>
			<hr>
				<div class="btn-group cart">
				<a href="{{ route('cubo.finalbuy', $products) }}"><button type="button" class="btn btn-success">
						Comprar 
					</button></a>
				</div>
			@else
			<div class="nostock">Lo sentimos, este artículo se encuentra actualmente fuera de stock</div>
			<hr>
				<div class="btn-group cart">
					<button type="button" class="btn btn-success" disabled>
						Comprar 
					</button>
				</div>
			@endif
			</div>
		</div> 
	</div>
</div>
<!-- FIN Mostrar el producto -->
@endsection