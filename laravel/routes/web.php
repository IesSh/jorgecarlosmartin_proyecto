<?php
use App\Http\Controllers\AdministradorController;

// ADMINISTRADOR PARA CRUD DE PRODUCTOS Y USUARIOS

Route::group(['middleware' => 'auth'], function() {
    
    //PRODUCTOS
    Route::get('/admin', [
        'uses' => 'AdministradorController@getIndex', 
        'as' => 'product.index'
    ]);
    
    Route::get('/admin/indexCreate', [
        'uses' => 'AdministradorController@indexCreate', 
        'as' => 'admin.create.index'
    ]);

    Route::post('/admin/create', [
        'uses' => 'AdministradorController@create', 
        'as' => 'admin.create'
    ]);

    Route::get('/admin/edit/{id}', [
        'uses' => 'AdministradorController@edit', 
        'as' => 'admin.edit'
    ]);

    Route::put('/admin/edit/{id}', [
        'uses' => 'AdministradorController@update', 
        'as' => 'admin.update'
    ]);

    Route::delete('/admin/delete/{id}', [
        'uses' => 'AdministradorController@delete', 
        'as' => 'admin.delete'
    ]);

    //USUARIOS
    Route::get('/admin/users', [
        'uses' => 'AdministradorController@indexUsers', 
        'as' => 'admin.users'
    ]);

    Route::get('/admin/indexUserCreate', [
        'uses' => 'AdministradorController@indexUserCreate', 
        'as' => 'admin.indexUserCreate'
    ]);

    Route::post('/admin/userCreate', [
        'uses' => 'AdministradorController@userCreate', 
        'as' => 'admin.userCreate'
    ]);

    Route::get('/admin/userEdit/{id}', [
        'uses' => 'AdministradorController@userEdit', 
        'as' => 'admin.userEdit'
    ]);

    Route::put('/admin/userUpdate/{id}', [
        'uses' => 'AdministradorController@userUpdate', 
        'as' => 'admin.userUpdate'
    ]);

    Route::delete('/admin/userDelete/{id}', [
        'uses' => 'AdministradorController@userDelete', 
        'as' => 'admin.userDelete'
    ]);
});

// FIN ADMINISTRADOR



// INDEX, PRODUCTOS, PARA TODOS LOS USUARIOS
Route::get('/', [
    'uses' => 'ProductController@getIndex', 
    'as' => 'product.index'
]);

Route::get('/workInProgress', [
    'uses' => 'ProductController@getLoading', 
    'as' => 'loading.index'
]);

Route::get('/cubo', [
    'uses' => 'CuboController@getIndex', 
    'as' => 'cubo.index'
]);

Route::get('/cubo/buy/{id}', [
    'uses' => 'CuboController@buy', 
    'as' => 'cubo.buy'
]);


// FIN INDEX



// GESTION DE USUARIOS

Route::group(['prefix' => 'user'], function() {
    Route::group(['middleware' => 'guest'], function() {
        Route::get('/signup', [
            'uses' => 'UserController@getSignup',
            'as' => 'user.signup'
        ]);

        Route::post('/signup', [
            'uses' => 'UserController@postSignup',
            'as' => 'user.signup'
        ]);

        Route::get('/signin', [
            'uses' => 'UserController@getSignin',
            'as' => 'user.signin'
        ]);

        Route::post('/signin', [
            'uses' => 'UserController@postSignin',
            'as' => 'user.signin'
        ]);
    });
    //CUANDO YA ESTAN REGISTRADOS E INICIADO SESION
    Route::group(['middleware' => 'auth'], function() {
        Route::get('/profile', [
            'uses' => 'UserController@getProfile',
            'as' => 'user.profile'
        ]);

        //COMPRAR 
        Route::get('/cubo/finalbuy/{id}', [
            'uses' => 'CuboController@finalbuy', 
            'as' => 'cubo.finalbuy'
        ]);

        Route::get('/cubo/finalbuyValidate/{id}', [
            'uses' => 'CuboController@finalbuyValidate', 
            'as' => 'cubo.finalbuyValidate'
        ]);

        //FIN COMPRAR

        Route::get('/profile/edit/{id}', [
            'uses' => 'UserController@userEdit',
            'as' => 'user.edit'
        ]);

        Route::put('/profile/update/{id}', [
            'uses' => 'UserController@userUpdate', 
            'as' => 'user.update'
        ]);

        Route::delete('/profile/delete/{id}', [
            'uses' => 'UserController@userDelete', 
            'as' => 'user.delete'
        ]);
        
        Route::get('/logout', [
            'uses' => 'UserController@getLogout',
            'as' => 'user.logout'
        ]);

        
    });
});

// FIN GESTION DE USUARIO

