<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Auth;
use File;
use DB;

class UserController extends Controller
{
    public function getSignup() {
        return view('user.signup');
    }

    public function postSignup(Request $request) {
        $this->validate($request, [
            'name' => 'required|regex:/^[a-zA-Z\s]*$/|min:2',
            'email' => 'required|regex:/[\w-\.]+@([\w-]+\.)+[\w-]{2,4}/|unique:users,email',
            'password' => 'required|min:4',
            'address' => 'required|min:3',
            'city' => 'required|regex:/^[a-zA-Z\s]*$/|min:3',
            'zip' => 'required|min:5|max:5|regex:/^[0-9]*$/',
        ]);
        $user = new User([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'address' => $request->input('address'),
            'city' => $request->input('city'),
            'zip' => $request->input('zip')
        ]);
        $user->save();

        Auth::login($user);

        return redirect()->route('user.profile');
    }

    public function getSignin() {
        return view('user.signin');
    }

    public function postSignin(Request $request) {
        $this->validate($request, [
            'email' => 'email|required',
            'password' => 'required|min:4'
        ]);
        
        $user_data = array(
            'email' => $request->input('email'),
            'password' => $request->input('password')
        );

        if(Auth::attempt($user_data)){
            return redirect()->route('user.profile');
        } else {
            return back()->with('error', 'Las credenciales no son correctas');
        }
    }

    public function getProfile() {
        $user = User::all();
        return view('user.profile', compact('user'));
    }

    public function userEdit($id) {
        $user = User::findOrFail($id);
        return view('user.edit', compact('user'));
    }

    public function userUpdate(Request $request, $id){
        $request->validate([
            'name' => 'required|regex:/^[a-zA-Z\s]*$/|min:2',
            'email' => "required|regex:/[\w-\.]+@([\w-]+\.)+[\w-]{2,4}/|unique:users,email,$id",
            'address' => 'required|min:3',
            'city' => 'required|regex:/^[a-zA-Z\s]*$/|min:3',
            'zip' => 'required|min:5|max:5|regex:/^[0-9]*$/',
        ]);

        $Update = User::findOrFail($id);
        $Update->name = $request->name;
        $Update->email = $request->email;
        $Update->address = $request->address;
        $Update->city = $request->city;
        $Update->zip = $request->zip;
        
        $Update->save();
        
        return back()->with('mensaje', 'Usuario editado con exito');
    }
    
    public function getLogout() {
        Auth::logout();
        return redirect()->back();
    }
    public function userDelete($id){
        $usuarioEliminar = User::findOrFail($id);
        $usuarioEliminar->delete();

        return back()->with('mensaje', 'Usuario eliminado');
    }
}
