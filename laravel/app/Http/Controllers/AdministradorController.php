<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\User;
use File;
use DB;

class AdministradorController extends Controller
{   
    public function __construct(){
        $this->middleware('EsAdmin');
    }
    
    public function getIndex(Request $request)
    {
        if($request) {
            $query = $request->get('buscarpor');
            $tipo = $request->get('tipo');
            if($tipo != null) {
                $products = Product::where($tipo, 'LIKE', '%'. $query . '%' )
                ->orderBy('id', 'asc')
                ->get();
            } else {
                $products = Product::all();
                return view('admin.index', ['products' => $products, 'buscarpor' => $query]);
            }
            return view('admin.index', ['products' => $products, 'buscarpor' => $query]);
        }
    }

    public function indexCreate()
    {
        $products = Product::all();
        return view('admin.create', ['products' => $products]);
    }

    public function create(Request $request) {
      
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'price' => 'required|numeric|min:1',
            'file' => 'required',
            'stock' => 'required|numeric|min:0', 
        ]);

        $entrada=$request->all();

        if ($archivo=$request->file('file')){
            $nombre=$archivo->getClientOriginalName();
            $nombre = 'img/'.$nombre;
            $archivo->move('img', $nombre );
            $entrada['imagePath']=$nombre;
        }

        Product::create($entrada);
        return back()->with('mensaje', 'Producto creado con exito');
    }

    public function edit($id) {
        $product = Product::findOrFail($id);
        return view('admin.edit', compact('product'));
    }
    public function update(Request $request, $id){
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'price' => 'required|numeric|min:1',
            'stock' => 'required|numeric|min:0', 
        ]);

        $productUpdate = Product::findOrFail($id);
        $productUpdate->title = $request->title;
        $productUpdate->description = $request->description;
        $productUpdate->price = $request->price;
        $productUpdate->stock = $request->stock;
        
        $productUpdate->save();

        return back()->with('mensaje', 'Producto editado con exito');
    }

    public function delete($id){
        $productEliminar = Product::findOrFail($id);
        $productEliminar->delete();

        return back()->with('mensaje', 'Producto eliminado con exito');
    }


    public function indexUsers() {
        $users = User::all();
        return view('admin.users', ['users' => $users]);
    }

    public function indexUserCreate(){
        return view('admin.userCreate');
    }

    public function userCreate(Request $request){
        $this->validate($request, [
            'name' => 'required|regex:/^[a-zA-Z\s]*$/|min:2',
            'email' => 'required|regex:/[\w-\.]+@([\w-]+\.)+[\w-]{2,4}/|unique:users,email',
            'password' => 'required|min:4',
            'address' => 'required|min:3',
            'role_id' => 'required|regex:/^[1-2]$/',
            'city' => 'required|regex:/^[a-zA-Z\s]*$/|min:3',
            'zip' => 'required|min:5|max:5|regex:/^[0-9]*$/',
        ]);
        $user = new User([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'address' => $request->input('address'),
            'role_id' => $request->input('role_id'),
            'city' => $request->input('city'),
            'zip' => $request->input('zip')
        ]);
        $user->save();

        return back()->with('mensaje', 'Usuario creado con exito');
    }

    public function userEdit($id) {
        $user = User::findOrFail($id);
        return view('admin.userEdit', compact('user'));
    }

    public function userUpdate(Request $request, $id){
        $request->validate([
            'name' => 'required|regex:/^[a-zA-Z\s]*$/|min:2',
            'email' => "required|regex:/[\w-\.]+@([\w-]+\.)+[\w-]{2,4}/|unique:users,email,$id",
            'address' => 'required|min:3',
            'role_id' => 'required|regex:/^[1-2]$/',
            'city' => 'required|regex:/^[a-zA-Z\s]*$/|min:3',
            'zip' => 'required|min:5|max:5|regex:/^[0-9]*$/',
        ]);

        $userUpdate = User::findOrFail($id);
        $userUpdate->name = $request->name;
        $userUpdate->email = $request->email;
        $userUpdate->role_id = $request->role_id;
        $userUpdate->address = $request->address;
        $userUpdate->city = $request->city;
        $userUpdate->zip = $request->zip;
        
        $userUpdate->save();

        return back()->with('mensaje', 'Usuario editado con exito');
    }
    public function userDelete($id){
        $userDelete = User::findOrFail($id);
        $userDelete->delete();

        return back()->with('mensaje', 'Usuario eliminado con exito');
    }
}


