<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  

    
    protected $fillable = [
        'name', 'email', 'role_id', 'password','address', 'city', 'zip'
    ];

    protected $hidden = [
        'password', 'rememeber_token',
  ];

    public function role() {
        return $this->belongsTo('App\Role');
    }

    public function esAdmin() {
        if($this->role->nombre_rol=='administrador') {
            return true;
        }
        return false;
    }

}
